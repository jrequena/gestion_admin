<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class PurchaseInvoice
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $delivery_date;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var float
     */
    private $total;

    /**
     * @var float
     */
    private $iva;

    /**
     * @var \DateTime
     */
    private $last_update;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $purchased_product;

    /**
     * @var \AppBundle\Entity\Provider
     */
    private $provider;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->purchased_product = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deliveryDate
     *
     * @param \DateTime $deliveryDate
     *
     * @return PurchaseInvoice
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->delivery_date = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->delivery_date;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return PurchaseInvoice
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return PurchaseInvoice
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set iva
     *
     * @param float $iva
     *
     * @return PurchaseInvoice
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return float
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return PurchaseInvoice
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return PurchaseInvoice
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Add purchasedProduct
     *
     * @param \AppBundle\Entity\PurchasedProduct $purchasedProduct
     *
     * @return PurchaseInvoice
     */
    public function addPurchasedProduct(\AppBundle\Entity\PurchasedProduct $purchasedProduct)
    {
        $this->purchased_product[] = $purchasedProduct;

        return $this;
    }

    /**
     * Remove purchasedProduct
     *
     * @param \AppBundle\Entity\PurchasedProduct $purchasedProduct
     */
    public function removePurchasedProduct(\AppBundle\Entity\PurchasedProduct $purchasedProduct)
    {
        $this->purchased_product->removeElement($purchasedProduct);
    }

    /**
     * Get purchasedProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchasedProduct()
    {
        return $this->purchased_product;
    }

    /**
     * Set provider
     *
     * @param \AppBundle\Entity\Provider $provider
     *
     * @return PurchaseInvoice
     */
    public function setProvider(\AppBundle\Entity\Provider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AppBundle\Entity\Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }
}
