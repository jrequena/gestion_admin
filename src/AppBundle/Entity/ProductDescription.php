<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class ProductDescription
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $lvl;

    /**
     * @var \DateTime
     */
    private $last_update;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $product_type;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $products_category;

    /**
     * @var \AppBundle\Entity\ProductDescription
     */
    private $parent;

    /**
     * @var \AppBundle\Entity\ProductDescription
     */
    private $root;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->product_type = new \Doctrine\Common\Collections\ArrayCollection();
        $this->products_category = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ProductDescription
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return ProductDescription
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return ProductDescription
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ProductDescription
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\ProductDescription $child
     *
     * @return ProductDescription
     */
    public function addChild(\AppBundle\Entity\ProductDescription $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\ProductDescription $child
     */
    public function removeChild(\AppBundle\Entity\ProductDescription $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add productType
     *
     * @param \AppBundle\Entity\Product $productType
     *
     * @return ProductDescription
     */
    public function addProductType(\AppBundle\Entity\Product $productType)
    {
        $this->product_type[] = $productType;

        return $this;
    }

    /**
     * Remove productType
     *
     * @param \AppBundle\Entity\Product $productType
     */
    public function removeProductType(\AppBundle\Entity\Product $productType)
    {
        $this->product_type->removeElement($productType);
    }

    /**
     * Get productType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductType()
    {
        return $this->product_type;
    }

    /**
     * Add productsCategory
     *
     * @param \AppBundle\Entity\Product $productsCategory
     *
     * @return ProductDescription
     */
    public function addProductsCategory(\AppBundle\Entity\Product $productsCategory)
    {
        $this->products_category[] = $productsCategory;

        return $this;
    }

    /**
     * Remove productsCategory
     *
     * @param \AppBundle\Entity\Product $productsCategory
     */
    public function removeProductsCategory(\AppBundle\Entity\Product $productsCategory)
    {
        $this->products_category->removeElement($productsCategory);
    }

    /**
     * Get productsCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsCategory()
    {
        return $this->products_category;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\ProductDescription $parent
     *
     * @return ProductDescription
     */
    public function setParent(\AppBundle\Entity\ProductDescription $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\ProductDescription
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set root
     *
     * @param \AppBundle\Entity\ProductDescription $root
     *
     * @return ProductDescription
     */
    public function setRoot(\AppBundle\Entity\ProductDescription $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return \AppBundle\Entity\ProductDescription
     */
    public function getRoot()
    {
        return $this->root;
    }
}
