<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class NaturalCustomer
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $identity_card;

    /**
     * @var string
     */
    private $firt_name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \DateTime
     */
    private $last_update;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identityCard
     *
     * @param string $identityCard
     *
     * @return NaturalCustomer
     */
    public function setIdentityCard($identityCard)
    {
        $this->identity_card = $identityCard;

        return $this;
    }

    /**
     * Get identityCard
     *
     * @return string
     */
    public function getIdentityCard()
    {
        return $this->identity_card;
    }

    /**
     * Set firtName
     *
     * @param string $firtName
     *
     * @return NaturalCustomer
     */
    public function setFirtName($firtName)
    {
        $this->firt_name = $firtName;

        return $this;
    }

    /**
     * Get firtName
     *
     * @return string
     */
    public function getFirtName()
    {
        return $this->firt_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return NaturalCustomer
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return NaturalCustomer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return NaturalCustomer
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return NaturalCustomer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return NaturalCustomer
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return NaturalCustomer
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sales_invoices;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sales_invoices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add salesInvoice
     *
     * @param \AppBundle\Entity\SalesInvoice $salesInvoice
     *
     * @return NaturalCustomer
     */
    public function addSalesInvoice(\AppBundle\Entity\SalesInvoice $salesInvoice)
    {
        $this->sales_invoices[] = $salesInvoice;

        return $this;
    }

    /**
     * Remove salesInvoice
     *
     * @param \AppBundle\Entity\SalesInvoice $salesInvoice
     */
    public function removeSalesInvoice(\AppBundle\Entity\SalesInvoice $salesInvoice)
    {
        $this->sales_invoices->removeElement($salesInvoice);
    }

    /**
     * Get salesInvoices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSalesInvoices()
    {
        return $this->sales_invoices;
    }
}
