<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class Product
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $bar_code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $measuring_unit;

    /**
     * @var string
     */
    private $color;

    /**
     * @var string
     */
    private $total_amount;

    /**
     * @var float
     */
    private $size;

    /**
     * @var integer
     */
    private $min_amount;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $is_active;

    /**
     * @var \DateTime
     */
    private $last_update;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $prices;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $movements;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $excluded_products;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $purchased_products;

    /**
     * @var \AppBundle\Entity\ProductDescription
     */
    private $type;

    /**
     * @var \AppBundle\Entity\ProductDescription
     */
    private $category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->prices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->movements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->excluded_products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->purchased_products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barCode
     *
     * @param string $barCode
     *
     * @return Product
     */
    public function setBarCode($barCode)
    {
        $this->bar_code = $barCode;

        return $this;
    }

    /**
     * Get barCode
     *
     * @return string
     */
    public function getBarCode()
    {
        return $this->bar_code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set measuringUnit
     *
     * @param string $measuringUnit
     *
     * @return Product
     */
    public function setMeasuringUnit($measuringUnit)
    {
        $this->measuring_unit = $measuringUnit;

        return $this;
    }

    /**
     * Get measuringUnit
     *
     * @return string
     */
    public function getMeasuringUnit()
    {
        return $this->measuring_unit;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Product
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set totalAmount
     *
     * @param string $totalAmount
     *
     * @return Product
     */
    public function setTotalAmount($totalAmount)
    {
        $this->total_amount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return string
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * Set size
     *
     * @param float $size
     *
     * @return Product
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return float
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set minAmount
     *
     * @param integer $minAmount
     *
     * @return Product
     */
    public function setMinAmount($minAmount)
    {
        $this->min_amount = $minAmount;

        return $this;
    }

    /**
     * Get minAmount
     *
     * @return integer
     */
    public function getMinAmount()
    {
        return $this->min_amount;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Product
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Product
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Product
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\ProductGallery $image
     *
     * @return Product
     */
    public function addImage(\AppBundle\Entity\ProductGallery $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\ProductGallery $image
     */
    public function removeImage(\AppBundle\Entity\ProductGallery $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add price
     *
     * @param \AppBundle\Entity\ProductPrices $price
     *
     * @return Product
     */
    public function addPrice(\AppBundle\Entity\ProductPrices $price)
    {
        $this->prices[] = $price;

        return $this;
    }

    /**
     * Remove price
     *
     * @param \AppBundle\Entity\ProductPrices $price
     */
    public function removePrice(\AppBundle\Entity\ProductPrices $price)
    {
        $this->prices->removeElement($price);
    }

    /**
     * Get prices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Add movement
     *
     * @param \AppBundle\Entity\ProductMovement $movement
     *
     * @return Product
     */
    public function addMovement(\AppBundle\Entity\ProductMovement $movement)
    {
        $this->movements[] = $movement;

        return $this;
    }

    /**
     * Remove movement
     *
     * @param \AppBundle\Entity\ProductMovement $movement
     */
    public function removeMovement(\AppBundle\Entity\ProductMovement $movement)
    {
        $this->movements->removeElement($movement);
    }

    /**
     * Get movements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovements()
    {
        return $this->movements;
    }

    /**
     * Add excludedProduct
     *
     * @param \AppBundle\Entity\ExcludedProduct $excludedProduct
     *
     * @return Product
     */
    public function addExcludedProduct(\AppBundle\Entity\ExcludedProduct $excludedProduct)
    {
        $this->excluded_products[] = $excludedProduct;

        return $this;
    }

    /**
     * Remove excludedProduct
     *
     * @param \AppBundle\Entity\ExcludedProduct $excludedProduct
     */
    public function removeExcludedProduct(\AppBundle\Entity\ExcludedProduct $excludedProduct)
    {
        $this->excluded_products->removeElement($excludedProduct);
    }

    /**
     * Get excludedProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExcludedProducts()
    {
        return $this->excluded_products;
    }

    /**
     * Add purchasedProduct
     *
     * @param \AppBundle\Entity\PurchasedProduct $purchasedProduct
     *
     * @return Product
     */
    public function addPurchasedProduct(\AppBundle\Entity\PurchasedProduct $purchasedProduct)
    {
        $this->purchased_products[] = $purchasedProduct;

        return $this;
    }

    /**
     * Remove purchasedProduct
     *
     * @param \AppBundle\Entity\PurchasedProduct $purchasedProduct
     */
    public function removePurchasedProduct(\AppBundle\Entity\PurchasedProduct $purchasedProduct)
    {
        $this->purchased_products->removeElement($purchasedProduct);
    }

    /**
     * Get purchasedProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchasedProducts()
    {
        return $this->purchased_products;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\ProductDescription $type
     *
     * @return Product
     */
    public function setType(\AppBundle\Entity\ProductDescription $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\ProductDescription
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\ProductDescription $category
     *
     * @return Product
     */
    public function setCategory(\AppBundle\Entity\ProductDescription $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\ProductDescription
     */
    public function getCategory()
    {
        return $this->category;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sold_products;


    /**
     * Add soldProduct
     *
     * @param \AppBundle\Entity\SoldProduct $soldProduct
     *
     * @return Product
     */
    public function addSoldProduct(\AppBundle\Entity\SoldProduct $soldProduct)
    {
        $this->sold_products[] = $soldProduct;

        return $this;
    }

    /**
     * Remove soldProduct
     *
     * @param \AppBundle\Entity\SoldProduct $soldProduct
     */
    public function removeSoldProduct(\AppBundle\Entity\SoldProduct $soldProduct)
    {
        $this->sold_products->removeElement($soldProduct);
    }

    /**
     * Get soldProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSoldProducts()
    {
        return $this->sold_products;
    }
}
