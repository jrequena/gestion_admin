<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class LegalCustomer
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $rif;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \DateTime
     */
    private $last_update;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LegalCustomer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rif
     *
     * @param string $rif
     *
     * @return LegalCustomer
     */
    public function setRif($rif)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return string
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return LegalCustomer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return LegalCustomer
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return LegalCustomer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return LegalCustomer
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return LegalCustomer
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sales_invoices;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sales_invoices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add salesInvoice
     *
     * @param \AppBundle\Entity\SalesInvoice $salesInvoice
     *
     * @return LegalCustomer
     */
    public function addSalesInvoice(\AppBundle\Entity\SalesInvoice $salesInvoice)
    {
        $this->sales_invoices[] = $salesInvoice;

        return $this;
    }

    /**
     * Remove salesInvoice
     *
     * @param \AppBundle\Entity\SalesInvoice $salesInvoice
     */
    public function removeSalesInvoice(\AppBundle\Entity\SalesInvoice $salesInvoice)
    {
        $this->sales_invoices->removeElement($salesInvoice);
    }

    /**
     * Get salesInvoices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSalesInvoices()
    {
        return $this->sales_invoices;
    }
}
