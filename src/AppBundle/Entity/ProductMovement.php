<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class ProductMovement
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var string
     */
    private $sign;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $last_update;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \AppBundle\Entity\ExcludedProduct
     */
    private $excluded_product;

    /**
     * @var \AppBundle\Entity\PurchasedProduct
     */
    private $purchased_product;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product;

    /**
     * @var \AppBundle\Entity\Storehouse
     */
    private $storehouse;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return ProductMovement
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set sign
     *
     * @param string $sign
     *
     * @return ProductMovement
     */
    public function setSign($sign)
    {
        $this->sign = $sign;

        return $this;
    }

    /**
     * Get sign
     *
     * @return string
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ProductMovement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProductMovement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return ProductMovement
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ProductMovement
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set excludedProduct
     *
     * @param \AppBundle\Entity\ExcludedProduct $excludedProduct
     *
     * @return ProductMovement
     */
    public function setExcludedProduct(\AppBundle\Entity\ExcludedProduct $excludedProduct = null)
    {
        $this->excluded_product = $excludedProduct;

        return $this;
    }

    /**
     * Get excludedProduct
     *
     * @return \AppBundle\Entity\ExcludedProduct
     */
    public function getExcludedProduct()
    {
        return $this->excluded_product;
    }

    /**
     * Set purchasedProduct
     *
     * @param \AppBundle\Entity\PurchasedProduct $purchasedProduct
     *
     * @return ProductMovement
     */
    public function setPurchasedProduct(\AppBundle\Entity\PurchasedProduct $purchasedProduct = null)
    {
        $this->purchased_product = $purchasedProduct;

        return $this;
    }

    /**
     * Get purchasedProduct
     *
     * @return \AppBundle\Entity\PurchasedProduct
     */
    public function getPurchasedProduct()
    {
        return $this->purchased_product;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return ProductMovement
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set storehouse
     *
     * @param \AppBundle\Entity\Storehouse $storehouse
     *
     * @return ProductMovement
     */
    public function setStorehouse(\AppBundle\Entity\Storehouse $storehouse = null)
    {
        $this->storehouse = $storehouse;

        return $this;
    }

    /**
     * Get storehouse
     *
     * @return \AppBundle\Entity\Storehouse
     */
    public function getStorehouse()
    {
        return $this->storehouse;
    }
    /**
     * @var \AppBundle\Entity\SoldProduct
     */
    private $sold_product;


    /**
     * Set soldProduct
     *
     * @param \AppBundle\Entity\SoldProduct $soldProduct
     *
     * @return ProductMovement
     */
    public function setSoldProduct(\AppBundle\Entity\SoldProduct $soldProduct = null)
    {
        $this->sold_product = $soldProduct;

        return $this;
    }

    /**
     * Get soldProduct
     *
     * @return \AppBundle\Entity\SoldProduct
     */
    public function getSoldProduct()
    {
        return $this->sold_product;
    }
}
