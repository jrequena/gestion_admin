<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class SalesInvoice
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $total_amount;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sold_products;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sold_products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     *
     * @return SalesInvoice
     */
    public function setTotalAmount($totalAmount)
    {
        $this->total_amount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * Add soldProduct
     *
     * @param \AppBundle\Entity\SoldProduct $soldProduct
     *
     * @return SalesInvoice
     */
    public function addSoldProduct(\AppBundle\Entity\SoldProduct $soldProduct)
    {
        $this->sold_products[] = $soldProduct;

        return $this;
    }

    /**
     * Remove soldProduct
     *
     * @param \AppBundle\Entity\SoldProduct $soldProduct
     */
    public function removeSoldProduct(\AppBundle\Entity\SoldProduct $soldProduct)
    {
        $this->sold_products->removeElement($soldProduct);
    }

    /**
     * Get soldProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSoldProducts()
    {
        return $this->sold_products;
    }
    /**
     * @var \AppBundle\Entity\LegalCustomer
     */
    private $legal_customer;

    /**
     * @var \AppBundle\Entity\NaturalCustomer
     */
    private $natural_customer;


    /**
     * Set legalCustomer
     *
     * @param \AppBundle\Entity\LegalCustomer $legalCustomer
     *
     * @return SalesInvoice
     */
    public function setLegalCustomer(\AppBundle\Entity\LegalCustomer $legalCustomer = null)
    {
        $this->legal_customer = $legalCustomer;

        return $this;
    }

    /**
     * Get legalCustomer
     *
     * @return \AppBundle\Entity\LegalCustomer
     */
    public function getLegalCustomer()
    {
        return $this->legal_customer;
    }

    /**
     * Set naturalCustomer
     *
     * @param \AppBundle\Entity\NaturalCustomer $naturalCustomer
     *
     * @return SalesInvoice
     */
    public function setNaturalCustomer(\AppBundle\Entity\NaturalCustomer $naturalCustomer = null)
    {
        $this->natural_customer = $naturalCustomer;

        return $this;
    }

    /**
     * Get naturalCustomer
     *
     * @return \AppBundle\Entity\NaturalCustomer
     */
    public function getNaturalCustomer()
    {
        return $this->natural_customer;
    }
}
