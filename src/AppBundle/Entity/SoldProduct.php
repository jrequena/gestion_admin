<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class SoldProduct
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $delivery_date;

    /**
     * @var \AppBundle\Entity\ProductMovement
     */
    private $movements;

    /**
     * @var \AppBundle\Entity\SalesInvoice
     */
    private $sales_invoice;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deliveryDate
     *
     * @param \DateTime $deliveryDate
     *
     * @return SoldProduct
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->delivery_date = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->delivery_date;
    }

    /**
     * Set movements
     *
     * @param \AppBundle\Entity\ProductMovement $movements
     *
     * @return SoldProduct
     */
    public function setMovements(\AppBundle\Entity\ProductMovement $movements = null)
    {
        $this->movements = $movements;

        return $this;
    }

    /**
     * Get movements
     *
     * @return \AppBundle\Entity\ProductMovement
     */
    public function getMovements()
    {
        return $this->movements;
    }

    /**
     * Set salesInvoice
     *
     * @param \AppBundle\Entity\SalesInvoice $salesInvoice
     *
     * @return SoldProduct
     */
    public function setSalesInvoice(\AppBundle\Entity\SalesInvoice $salesInvoice = null)
    {
        $this->sales_invoice = $salesInvoice;

        return $this;
    }

    /**
     * Get salesInvoice
     *
     * @return \AppBundle\Entity\SalesInvoice
     */
    public function getSalesInvoice()
    {
        return $this->sales_invoice;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return SoldProduct
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
