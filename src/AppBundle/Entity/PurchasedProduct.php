<?php
namespace AppBundle\Entity;

/**
 * Job
 *
 * La clase representa el trabajo buscado por
 * los usuarios en el sistema, donde se da detalle de
 * lo que quiere realizar el usuario en el inmueble
 *
 * @author Freddy Contreras
 */
class PurchasedProduct
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $total_amount;

    /**
     * @var float
     */
    private $sell_rate;

    /**
     * @var float
     */
    private $iva;

    /**
     * @var \DateTime
     */
    private $last_update;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \AppBundle\Entity\ProductMovement
     */
    private $movements;

    /**
     * @var \AppBundle\Entity\PurchaseInvoice
     */
    private $purchase_invoice;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     *
     * @return PurchasedProduct
     */
    public function setTotalAmount($totalAmount)
    {
        $this->total_amount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * Set sellRate
     *
     * @param float $sellRate
     *
     * @return PurchasedProduct
     */
    public function setSellRate($sellRate)
    {
        $this->sell_rate = $sellRate;

        return $this;
    }

    /**
     * Get sellRate
     *
     * @return float
     */
    public function getSellRate()
    {
        return $this->sell_rate;
    }

    /**
     * Set iva
     *
     * @param float $iva
     *
     * @return PurchasedProduct
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return float
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return PurchasedProduct
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return PurchasedProduct
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set movements
     *
     * @param \AppBundle\Entity\ProductMovement $movements
     *
     * @return PurchasedProduct
     */
    public function setMovements(\AppBundle\Entity\ProductMovement $movements = null)
    {
        $this->movements = $movements;

        return $this;
    }

    /**
     * Get movements
     *
     * @return \AppBundle\Entity\ProductMovement
     */
    public function getMovements()
    {
        return $this->movements;
    }

    /**
     * Set purchaseInvoice
     *
     * @param \AppBundle\Entity\PurchaseInvoice $purchaseInvoice
     *
     * @return PurchasedProduct
     */
    public function setPurchaseInvoice(\AppBundle\Entity\PurchaseInvoice $purchaseInvoice = null)
    {
        $this->purchase_invoice = $purchaseInvoice;

        return $this;
    }

    /**
     * Get purchaseInvoice
     *
     * @return \AppBundle\Entity\PurchaseInvoice
     */
    public function getPurchaseInvoice()
    {
        return $this->purchase_invoice;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return PurchasedProduct
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
